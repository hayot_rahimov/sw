DB = 'warehousedb'
APP = 'warehouse'


class WarehouseDBRouter(object):
    def db_for_read(self, model, **hints):
        "Point all operations on app1 models to 'db'"
        from django.conf import settings
        if DB not in settings.DATABASES:
            return None
        if model._meta.app_label == APP:
            return DB
        return None

    def db_for_write(self, model, **hints):
        "Point all operations on app1 models to 'app'"
        from django.conf import settings
        if DB not in settings.DATABASES:
            return None
        if model._meta.app_label == APP:
            return DB
        return None

    def allow_relation(self, obj1, obj2, **hints):
        "Allow any relation if a model in app is involved"
        from django.conf import settings
        if DB not in settings.DATABASES:
            return None
        if obj1._meta.app_label == APP or obj2._meta.app_label == APP:
            return True
        return None

    def allow_syncdb(self, db, model):
        "Make sure the db app only appears on the 'app' db"
        from django.conf import settings
        if DB not in settings.DATABASES:
            return None
        if db == DB:
            return model._meta.app_label == APP
        elif model._meta.app_label == APP:
            return False
        return None
