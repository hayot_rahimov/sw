from rest_framework import serializers

from warehouse.models import Product, Inventory, Warehouse


class WarehouseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Warehouse
        fields = (
            'name',
            'id',
        )
        read_only_fields = (
            'id',
        )


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = (
            'name',
            'number',
            'id',
        )
        read_only_fields = (
            'id',
        )


class InventorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Inventory
        fields = (
            'warehouse',
            'product',
            'quantity',
            'order_id',
            'store_id',
            'id',
        )
        read_only_fields = (
            'id',
        )
