from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers

from warehouse.views import WarehouseViewSet, ProductViewSet, InventoryViewSet, ProductNumberLookup

router = routers.DefaultRouter()
router.register(r'warehouses', WarehouseViewSet, 'warehouses')
router.register(r'products', ProductViewSet, 'warehouses')
router.register(r'inventories', InventoryViewSet, 'inventories')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
app_name = 'warehouse'
urlpatterns = [
    url(r'', include(router.urls)),
]
