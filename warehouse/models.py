from django.db import models
from django.db.models import DO_NOTHING


# Create your models here.
from sw import constants


class Warehouse(models.Model):
    name = models.CharField(max_length=255, verbose_name="Name")

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=255, verbose_name="Name")
    number = models.CharField(max_length=255, null=False, blank=False, unique=True, verbose_name="Unique Number")

    def __str__(self):
        return self.name


class Inventory(models.Model):
    warehouse = models.ForeignKey("warehouse.Warehouse", null=True, on_delete=DO_NOTHING)
    product = models.ForeignKey("warehouse.Product", null=False, on_delete=DO_NOTHING)
    quantity = models.IntegerField(verbose_name="Quantity", default=0, null=False)
    order_id = models.CharField(max_length=255, null=True, blank=True, verbose_name="Order ID")
    store_id = models.CharField(max_length=255, null=True, blank=True, verbose_name="Store ID")
    status = models.CharField(max_length=255, null=True, blank=True, verbose_name="Status", choices=constants.Constants.STATUS.CHOICES)

    def __str__(self):
        return "%d \"%s\" in %s" % (self.quantity, self.product.name, self.warehouse.name)
