from django.apps import AppConfig


class WarehouseConfig(AppConfig):
    name = 'warehouse'
    app_name = 'warehouse'
