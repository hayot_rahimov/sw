from django.forms import ModelForm

from warehouse.models import Inventory


class InventoryForm(ModelForm):
    class Meta:
        model = Inventory
        exclude = ('id',)

    def clean(self):
        return self.cleaned_data
