# Create your views here.
from dal import autocomplete
from django.db.models import Sum
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from store.models import Order, OrderItems
from sw import constants
from warehouse.models import Warehouse, Inventory, Product
from warehouse.serializers import WarehouseSerializer, ProductSerializer, InventorySerializer


class WarehouseViewSet(ModelViewSet):
    queryset = Warehouse.objects.all()
    serializer_class = WarehouseSerializer


class ProductViewSet(ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class InventoryViewSet(ModelViewSet):
    queryset = Inventory.objects.all()
    serializer_class = InventorySerializer

    @action(detail=False, methods=['post'], url_path='check', url_name='check')
    def check(self, request, *args, **kwargs):
        print(request.data)
        result = list()
        datas = request.data if isinstance(request.data, list) else [request.data]
        for data in datas:
            product = Product.objects.filter(number__exact=data.get('product_number')).first()
            available_quantity = Inventory.objects.filter(product=product).aggregate(available_quantity=Sum("quantity"))
            result.append({'quantity': available_quantity.get('available_quantity'), 'product_number': product.number})
        return Response(data=result)

    @action(detail=False, methods=['post'], url_path='order_created', url_name='order_created')
    def order_created(self, request, *args, **kwargs):
        print(request.data)
        order = Order.objects.get(id=request.data.get('id'))
        # items = request.data.get('items')
        has_not_packed_item = False
        for item in OrderItems.objects.filter(order_id=order.id):
            if not item.status or item.status == constants.Constants.STATUS.UNHANDLED:
                available_quantity = Inventory.objects.filter(product__number__exact=item.product_number).aggregate(available_quantity=Sum("quantity")).get('available_quantity') or 0
                print('available_quantity=', available_quantity)
                if item.quantity <= available_quantity:
                    asked_quantity = item.quantity
                    for inventory in Inventory.objects.filter(product__number__exact=item.product_number):
                        inventory_quantity = asked_quantity
                        if asked_quantity > inventory.quantity:
                            inventory_quantity = inventory.quantity
                        asked_quantity = asked_quantity - inventory_quantity
                        Inventory(warehouse=inventory.warehouse, product=inventory.product, order_id=order.id, quantity=-inventory_quantity).save()
                        if not asked_quantity:
                            break
                    item.status = constants.Constants.STATUS.PACKING
                else:
                    item.status = constants.Constants.STATUS.UNHANDLED
                    has_not_packed_item = True
                item.save()
        order.status = constants.Constants.STATUS.PACKING if not has_not_packed_item else constants.Constants.STATUS.UNHANDLED
        order.save()
        return Response(data={"status": "ok"})


class ProductNumberLookup(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        if self.q:
            q = Product.objects.all().filter(name__icontains=self.q)
        else:
            q = Product.objects.all()
        return q

    def get_results(self, context):
        """Return data for the 'results' key of the response."""
        return [
            {
                'id': self.get_result_value(result),
                'value': result.number,
                'text': self.get_result_label(result),
                'selected_text': self.get_selected_result_label(result),
            } for result in context['object_list']
        ]

    def get_result_value(self, result):
        return str(result.pk)
