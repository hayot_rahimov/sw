from django.contrib import admin

# Register your models here.
from django.urls import reverse

from sw.admin import CustomModelAdmin
from sw.api_calls import talk_to_api
from warehouse.forms import InventoryForm
from warehouse.models import Product, Warehouse, Inventory


class WarehouseAdmin(admin.ModelAdmin):
    pass


class InventoryAdmin(CustomModelAdmin):
    list_display = ("warehouse", "product", "order_id", 'quantity', 'status')
    form = InventoryForm

    def after_save(self, obj):
        obj = super(InventoryAdmin, self).after_save(obj)
        data = {"id": obj.id, "product_number": obj.product.number}
        reversed_url = reverse("store:orders-update_status", args=[obj.order_id])
        talk_to_api(self.get_request(), data=data, reversed_url=reversed_url, action='PUT')
        return obj


admin.site.register(Warehouse, WarehouseAdmin)
admin.site.register(Product)
admin.site.register(Inventory, InventoryAdmin)
