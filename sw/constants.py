from django.utils.translation import ugettext_lazy as _


class Constants:
    class STATUS:
        UNHANDLED = "UNHANDLED"  # customers have placed an order and it is already in our system, but the process of its execution has not started yet
        WAITING_FOR_SHIPMENT = "WAITING_FOR_SHIPMENT"  # this status appears only when a given product is not available in our stock and we must deliver it, for instance,
        EXECUTED = "EXECUTED"  # products are being collected
        PACKING = "PACKING"  # products have been delivered to a warehouse and are being packed
        READY_FOR_SHIPMENT = "READY_FOR_SHIPMENT"  # this status means that all products have been collected, they are waiting to be released to the courier
        SENT = "SENT"  # this status appears when products have been collected by the courier, after which the status of a given order may be checked in waybill
        COMPLETED = "COMPLETED"  # Completed

        DEFAULT = UNHANDLED
        CHOICES = (
            (UNHANDLED, _('Unhandled')),
            (WAITING_FOR_SHIPMENT, _('Waiting for shipment')),
            (EXECUTED, _('Executed')),
            (PACKING, _('Packing')),
            (READY_FOR_SHIPMENT, _('Ready for shipment')),
            (SENT, _('Sent')),
            (COMPLETED, _('Completed')),
        )
        AS_RESPONSE = {
            'choices': [{"name": y, "code": x} for x, y in CHOICES],
            'default': DEFAULT
        }

        @classmethod
        def get_int_version(cls, status):
            if status:
                if status == cls.WAITING_FOR_SHIPMENT:
                    return 1
                elif status == cls.EXECUTED:
                    return 2
                elif status == cls.PACKING:
                    return 3
                elif status == cls.READY_FOR_SHIPMENT:
                    return 4
                elif status == cls.SENT:
                    return 5
                elif status == cls.COMPLETED:
                    return 6
            return 0

        @staticmethod
        def get_lower(status1, status2):
            return status1 if Constants.STATUS.get_int_version(status1) < Constants.STATUS.get_int_version(status2) else status2
