from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework.utils import json


def talk_to_api(request, data=None, url="", action="GET", **kwargs):
    client = APIClient(SERVER_NAME='127.0.0.1:8000')
    user = request.user if request and getattr(request, 'user') and isinstance(getattr(request, 'user'), User) else User.objects.filter(is_staff=True).first()
    client.force_authenticate(user=user)  # we just need one user here.
    url = kwargs.get('reversed_url') if 'reversed_url' in kwargs else reverse(url)
    if action:
        if action.lower() == 'get':
            response = client.get(url, data=json.dumps(data), content_type='application/json')
        if action.lower() == 'post':
            response = client.post(url, data=json.dumps(data), content_type='application/json')
        if action.lower() == 'put':
            response = client.put(url, data=json.dumps(data), content_type='application/json')
        if response and response.status_code in (200, 201):
            print(response.status_code)
            return response.data
