from django.contrib.admin import ModelAdmin


class CustomModelAdmin(ModelAdmin):
    def __init__(self, *args, **kwargs):
        # let's define this so there's no chance of AttributeErrors
        self._request = None
        super(CustomModelAdmin, self).__init__(*args, **kwargs)

    def get_form(self, request, obj=None, change=False, **kwargs):
        form = super().get_form(request, obj, change, **kwargs)
        form.request = request
        return form

    def get_request(self):
        return self._request

    def changeform_view(self, request, *args, **kwargs):
        # stash the request
        self._request = request

        # call the parent view method with all the original args
        return super(CustomModelAdmin, self).changeform_view(request, *args, **kwargs)

    def response_add(self, request, obj, post_url_continue=None):
        obj = self.after_save(obj)
        return super(CustomModelAdmin, self).response_add(request, obj)

    def response_change(self, request, obj):
        obj = self.after_save(obj)
        return super(CustomModelAdmin, self).response_change(request, obj)

    def after_save(self, obj):
        # now we have what we need here... :)
        return obj
