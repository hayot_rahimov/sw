# make sure to install faker...
# pip install faker
import random
import time

from django.conf import settings
from faker import Faker

from store.models import Customer, Store
from warehouse.models import Warehouse, Product, Inventory

fake = Faker()


def seed_customers(num_entries=10, overwrite=False):
    """
    Creates num_entries worth a new users
    """
    if overwrite:
        print("Overwriting Customers")
        Customer.objects.all().delete()
    count = 0
    for _ in range(num_entries):
        first_name = fake.name_male()
        u = Customer(
            name=first_name,
            email=fake.first_name() + "@fakermail.com",
            phone=fake.phone_number()
        )
        u.save()
        count += 1
        percent_complete = count / num_entries * 100
        print(
            "Adding {} new Customers : {:.2f}%".format(num_entries, percent_complete),
            end='\r',
            flush=True
        )
    print()


def seed_store(num_entries=3, choice_min=2, choice_max=5, overwrite=False):
    """
    recreate stores
    """
    if overwrite:
        print('Overwriting Stores')
        Store.objects.all().delete()
    count = 0
    for _ in range(num_entries):
        p = Store(name="%s's Store" % fake.first_name())
        p.save()
        count += 1
        percent_complete = count / num_entries * 100
        print(
            "Adding {} new Stores: {:.2f}%".format(num_entries, percent_complete),
            end='\r',
            flush=True
        )
    print()


def seed_warehouse(num_entries=3, overwrite=False):
    """
    recreate warehouses
    """
    if overwrite:
        print('Overwriting Warehouses')
        Warehouse.objects.all().delete()
    count = 0
    for _ in range(num_entries):
        Warehouse(name="%s's Warehouse" % fake.first_name()).save()
        count += 1
        percent_complete = count / num_entries * 100
        print(
            "Adding {} new Warehouses: {:.2f}%".format(num_entries, percent_complete),
            end='\r',
            flush=True
        )
    print()


def seed_Product(product_names=None, overwrite=False):
    """
    recreate products
    """
    if overwrite:
        print('Overwriting Products')
        Warehouse.objects.all().delete()
    count = 0
    product_names = product_names or (
        'Jean',
        'Pen',
        'T-Shirts',
        'Coca-Cola',
        'Dirol',
        'Laptop',
        'Cap',
        'Cup',
        'Mouse',
    )

    def create_product(product_name):
        try:
            Product(name=product_name, number=random.randint(1, 199999)).save()
        except:
            create_product(product_name)

    for product_name in product_names:
        count += 1
        create_product(product_name)
        percent_complete = count / len(product_names) * 100
        print(
            "Adding {} new Products: {:.2f}%".format(len(product_names), percent_complete),
            end='\r',
            flush=True
        )
    print()


def seed_inventories():
    """
    Creates a new vote on every poll for every user
    Voted for choice is selected random.
    Deletes all votes prior to adding new ones
    """
    Inventory.objects.all().delete()
    products = Product.objects.all()
    warehouses = Warehouse.objects.all()
    count = 0
    number_of_new_inventories = warehouses.count() * products.count()
    for product in products:
        for warehouse in warehouses:
            Inventory(
                product=product,
                warehouse=warehouse,
                quantity=random.randint(1, 100)
            ).save()
            count += 1
            percent_complete = count / number_of_new_inventories * 100
            print(
                "Adding {} new inventories: {:.2f}%".format(number_of_new_inventories, percent_complete),
                end='\r',
                flush=True
            )
    print()


def seed_all(num_entries=10, overwrite=True):
    """
    Runs all seeder functions. Passes value of overwrite to all
    seeder function calls.
    """
    start_time = time.time()
    # run seeds
    seed_store(num_entries=3, overwrite=overwrite)
    seed_customers(num_entries=num_entries, overwrite=overwrite)
    seed_warehouse(num_entries=3, overwrite=overwrite)
    seed_Product(product_names=None, overwrite=overwrite)
    seed_inventories()
    # get time
    elapsed_time = time.time() - start_time
    minutes = int(elapsed_time // 60)
    seconds = int(elapsed_time % 60)
    print("Script Execution took: {} minutes {} seconds".format(minutes, seconds))


def __call__(*args, **kwargs):
    print("starting it")
    settings.configure()
