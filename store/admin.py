from django.contrib import admin
# Register your models here.
from django.forms import BaseInlineFormSet, forms

from store.forms import OrderForm, OrderItemForm
from store.models import Order, Store, OrderItems, Customer
from sw.admin import CustomModelAdmin
from sw.api_calls import talk_to_api


class OrderItemsInlineFormset(BaseInlineFormSet):
    def clean(self):
        # get forms that actually have valid data
        count = 0
        product_numbers = []
        for form in self.forms:
            try:
                if form.cleaned_data:
                    count += 1
                if form.cleaned_data and 'product_number' in form.cleaned_data and form.cleaned_data['product_number'] in product_numbers:
                    raise forms.ValidationError("One item with the same number is permitted.")
                if form.cleaned_data and 'product_number' in form.cleaned_data:
                    product_numbers.append(form.cleaned_data['product_number'])

            except AttributeError:
                # annoyingly, if a subform is invalid Django explicity raises
                # an AttributeError for cleaned_data
                pass
        if count < 1:
            raise forms.ValidationError('You must have at least one item')


class OrderItemsInline(admin.TabularInline):
    model = OrderItems
    extra = 0
    form = OrderItemForm
    formset = OrderItemsInlineFormset

    def get_form(self, request, obj=None, change=False, **kwargs):
        form = super().get_form(request, obj, change, **kwargs)
        form.request = request
        return form


class OrderAdmin(CustomModelAdmin):
    form = OrderForm
    inlines = [OrderItemsInline, ]
    list_display = ('id', 'customer', 'store', 'status')

    def after_save(self, obj):
        obj = super(OrderAdmin, self).after_save(obj)
        data = {"id": obj.id, "items": []}
        for item in obj.items.all():
            data.get("items").append({"id": item.id, "product_number": item.product_number, "quantity": item.quantity})
        talk_to_api(self.get_request(), data=data, url="warehouse:inventories-order_created", action='POST')
        return obj


admin.site.register(Store)
admin.site.register(Customer)
admin.site.register(Order, OrderAdmin)
