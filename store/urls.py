from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers
from rest_framework_nested.routers import NestedDefaultRouter

from store.views import OrderViewSet, OrderItemViewSet, StoreViewSet

router = routers.DefaultRouter()
router.register(r'orders', OrderViewSet, 'orders')
router.register(r'stores', StoreViewSet, 'stores')
item_router = NestedDefaultRouter(router, r'orders', lookup='item')
item_router.register(r'items', OrderItemViewSet, 'order_items')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
app_name = 'store'
urlpatterns = [
    url(r'', include(router.urls)),
]
