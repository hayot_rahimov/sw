from rest_framework import serializers

from store.models import Order, OrderItems, Store


class StoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = (
            'name',
            'id',
        )
        read_only_fields = (
            'id',
        )


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = (
            'status',
            'customer',
            'store',
            'id',
        )
        read_only_fields = (
            'id',
        )


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItems
        fields = (
            'product_number',
            'quantity',
            'order',
            'id',
        )
        read_only_fields = (
            'id',
        )
