from django.db import models
from django.db.models import DO_NOTHING

# Create your models here.
from sw import constants


class Customer(models.Model):
    name = models.CharField(max_length=255, verbose_name="Name")
    email = models.EmailField(max_length=255, null=True, verbose_name="Email")
    phone = models.CharField(max_length=255, null=True, verbose_name="Phone")

    def __str__(self):
        return self.name


class Store(models.Model):
    name = models.CharField(max_length=255, verbose_name="Name")

    def __str__(self):
        return self.name


class Order(models.Model):
    status = models.CharField(max_length=255, choices=constants.Constants.STATUS.CHOICES, default=constants.Constants.STATUS.DEFAULT)
    customer = models.ForeignKey("store.Customer", null=True, blank=True, on_delete=DO_NOTHING)
    store = models.ForeignKey("store.Store", null=True, blank=True, on_delete=DO_NOTHING)


class OrderItems(models.Model):
    product_number = models.CharField(verbose_name="Product Number", max_length=255, null=True)
    quantity = models.IntegerField(verbose_name="Quantity", default=0, null=False)
    order = models.ForeignKey("store.Order", null=False, related_name="items", on_delete=DO_NOTHING)
    status = models.CharField(max_length=255, choices=constants.Constants.STATUS.CHOICES, default=constants.Constants.STATUS.DEFAULT)

    def __str__(self):
        return self.product_number
