from dal import autocomplete
from django.core.exceptions import ValidationError
from django.db.models import Model
from django.forms import ModelChoiceField, ModelForm

from store.models import Order, OrderItems
from sw.api_calls import talk_to_api
from warehouse.models import Product


class ModelChoiceFieldCustom(ModelChoiceField):
    def to_python(self, value):
        if value in self.empty_values:
            return None
        try:
            key = self.to_field_name or 'pk'
            value = self.queryset.get(**{key: value})
        except (ValueError, TypeError, self.queryset.model.DoesNotExist):
            raise ValidationError(self.error_messages['invalid_choice'], code='invalid_choice')
        return value

    def prepare_value(self, value):
        if value =='2':
            raise Exception("see the stacktrace")
        if isinstance(value, str):
            value = self.queryset.filter(number__exact=value).first()
        if hasattr(value, '_meta'):
            if self.to_field_name:
                return value.serializable_value(self.to_field_name)
            else:
                return value.pk
        return super().prepare_value(value)

    def bound_data(self, data, initial):
        return initial


class OrderForm(ModelForm):
    class Meta:
        model = Order
        exclude = ('id',)

    def clean(self):
        return self.cleaned_data


class OrderItemForm(ModelForm):
    product_number = ModelChoiceFieldCustom(
        # to_field_name='number',
        queryset=Product.objects.all(),
        widget=autocomplete.ModelSelect2(url='product_number-autocomplete')
    )

    class Meta:
        model = OrderItems
        fields = ['product_number', 'quantity']

    def __init__(self, *args, **kwargs):
        super(OrderItemForm, self).__init__(*args, **kwargs)
        if self.instance and self.instance.product_number is not None and isinstance(self.instance.product_number, str):
            self.fields['product_number'].initial = Product.objects.filter(number__exact=self.instance.product_number).first()

    def clean(self):
        if 'product_number' in self.cleaned_data and self.cleaned_data['product_number'] and isinstance(self.cleaned_data['product_number'], Model):
            self.cleaned_data['product_number'] = self.cleaned_data['product_number'].number
        if self.cleaned_data['quantity'] <= 0 or 'product_number' not in self.cleaned_data or not self.cleaned_data['product_number']:
            self.cleaned_data['DELETE'] = True
        else:
            available_quantity = self.get_available_quantity_via_api() or 0
            if available_quantity < self.cleaned_data['quantity']:
                self.cleaned_data['product_number'] = Product.objects.filter(number__exact=self.cleaned_data['product_number']).first()
                raise ValidationError("No Such quantity on warehouse.")
        return self.cleaned_data

    def get_available_quantity_via_api(self):
        data = {
            "product_number": self.cleaned_data['product_number'],
            "quantity": self.cleaned_data['quantity']
        }
        response_data = talk_to_api(getattr(self, 'request', None), data, "warehouse:inventories-check", "POST")
        if response_data and isinstance(response_data, list):
            response_data = response_data[0]
        if response_data and isinstance(response_data, dict):
            result = response_data.get('quantity', -1)
            return result
        return -1
