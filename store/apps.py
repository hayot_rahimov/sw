from django.apps import AppConfig


class StoreConfig(AppConfig):
    name = 'store'
    app_name = 'store'
