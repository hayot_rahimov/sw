# Create your views here.
from rest_framework import permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from store.models import Order, Store, OrderItems
from store.serializers import OrderSerializer, StoreSerializer, OrderItemSerializer
from sw import constants
from warehouse.models import Inventory


class OrderViewSet(ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = (permissions.AllowAny,)

    @action(detail=True, methods=['put'], url_path='update_status', url_name='update_status')
    def update_status(self, request, *args, **kwargs):
        order = self.get_object()
        order_status = order.status
        order_new_status = None
        for item in order.items.all():
            item_status = item.status
            item_new_status = None
            for inventory in Inventory.objects.filter(order_id=order.id, product__number__exact=item.product_number):
                item_new_status = inventory.status if item_new_status is None else constants.Constants.STATUS.get_lower(item_new_status, inventory.status or constants.Constants.STATUS.UNHANDLED)
            item.status = item_new_status
            item.save()
            order_new_status = item.status if order_new_status is None else constants.Constants.STATUS.get_lower(order_new_status, item.status)
        order.status = order_new_status
        order.save()
        return Response(data={"status": "ok"})


class OrderItemViewSet(ModelViewSet):
    queryset = OrderItems.objects.all()
    serializer_class = OrderItemSerializer
    permission_classes = (permissions.AllowAny,)

    def get_queryset(self):
        q = OrderItems.objects.filter(order_id=self.kwargs.get('item_pk'))
        return q


class StoreViewSet(ModelViewSet):
    queryset = Store.objects.all()
    serializer_class = StoreSerializer
